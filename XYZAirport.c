/*
 ============================================================================
 Name        : XYZAirport.c
 Author      : Andrius Korsakas
 Version     :
 Copyright   : Free
 Description : XYZAirport
 Comments	 : Done on linux
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define UK 1
#define RESTOFEUROPE 2
#define ASIA 3
#define AMERICAS 4
#define AUSTRALASIA 5
#define ECONOMY 1
#define PREMIUM 2
#define BUSSINES 3
#define FIRST 4
#define THREE_TIMES 1
#define FIVE_TIMES 2
#define MORE_THAN_FIVE 3
#define ONE_DAY 1
#define THREE_DAYS 2
#define SEVEN_DAYS 3
#define MORE_THAN_SEVEN 4

//Passenger structure
typedef struct Passenger{
	int passportNumber;
	char firstName[20];
	char secondName[20];
	int DOB;
	char email[30];
	int travelOrigin;
	int travelClass;
	int travelPerYear;
	int durationOfStay;
	struct Passenger * next;
}Passenger;

int login();
void addPassenger(struct Passenger** head);
struct Passenger * createPassenger(Passenger * head, int passportNum);
void displayList(struct Passenger* head);
int searchPassengerList(Passenger * head, int passportNum, char fullName[]);
void passportUnique(int passportNum, Passenger * head);
void sortList(Passenger ** head);
Passenger * swapNodeData(Passenger * temp1, Passenger * temp2);
void updatePassenger(Passenger ** head, int position);
void loadPassengerListFile(Passenger ** head);
void displayPassenger(Passenger ** head, int update);
void printPassengerDetails(Passenger * head, int position);
void deletePassenger(Passenger ** head);
int listLength(Passenger * head);
int validateEmail(char email[]);
int * statisticsMenu(int statisticsOptions[]);
int statisticsSecondMenu();
void generateStatisticsProcess(Passenger * head, int statOptions[]);
double generateStatsByTravelClassAndTravelOrigin(Passenger ** head, int travelClass, int option);
void printPassengerReportFile(Passenger * head);

void main() {

	struct Passenger* headPtr = NULL;
	int choice;
	int loggedIn = 0;
	int statisticsOptions[3];

	//Loop for login verification
	do
	{
		loggedIn = login();
	}
	while(loggedIn == 0);

	loadPassengerListFile(&headPtr);
	sortList(&headPtr);

	printf("1. Add new passenger.\n");
	printf("2. Display all passengers to screen.\n");
	printf("3. Display a passengers details.\n");
	printf("4. Update a passenger.\n");
	printf("5. Delete a passenger.\n");
	printf("6. Generate statistics.\n");
	printf("7. Print all passengers details to report file.\n");

	scanf("%d", &choice);

	while(choice != -1)
	{
		switch(choice)
		{
		case 1:
			addPassenger(&headPtr);
			break;
		case 2:
			displayList(headPtr);
			break;
		case 3:
			displayPassenger(&headPtr, 0);
			break;
		case 4:
			displayPassenger(&headPtr, 1);
			break;
		case 5:
			deletePassenger(&headPtr);
			break;
		case 6:
			statisticsMenu(statisticsOptions);
			generateStatisticsProcess(headPtr, statisticsOptions);
			//printf("---- %lf ---", generateStatsByTravelClassAndTravelOrigin(&headPtr, statisticsOptions[1], statisticsOptions[2]));
			//printf("%d %d %d\n", statisticsOptions[0], statisticsOptions[1], statisticsOptions[2]);
			break;
		case 7:
			printPassengerReportFile(headPtr);
			break;
		}
		printf("1. Add new passenger.\n");
		printf("2. Display all passengers to screen.\n");
		printf("3. Display a passengers details.\n");
		printf("4. Update a passenger.\n");
		printf("5. Delete a passenger.\n");
		printf("6. Generate statistics.\n");
		printf("7. Print all passengers details to report file.\n");

		scanf("%d", &choice);
	}
}

//Function for printing passengers details to report file
void printPassengerReportFile(Passenger * head)
{
	FILE* fptr;

	Passenger* curr;

	int passengerNumber = 0;
	curr = head;
	fptr = fopen("passengersReport.txt", "w");

	if(fptr == NULL)
	{
		printf("Couldnt create file for writing.");
		exit(0);
	}
	else
	{
		//While not end of linked list, print passenger details to file
		while (curr != NULL)
		{
			passengerNumber++;
			fprintf(fptr, "Passenger No.%d:\n", passengerNumber);
			fprintf(fptr, "Passport number:  %d\n", curr->passportNumber);
			fprintf(fptr, "First name:       %s\n", curr->firstName);
			fprintf(fptr, "Last name:        %s\n", curr->secondName);
			fprintf(fptr, "DOB:              %d\n", curr->DOB);
			fprintf(fptr, "Email:            %s\n", curr->email);
			fprintf(fptr, "Origin of travel: ");
			//Switch statemnt to print correct info to file depending on options number code
			switch(curr->travelOrigin)
			{
			case UK:
				fprintf(fptr, "UK\n");
				break;
			case RESTOFEUROPE:
				fprintf(fptr, "Rest of Europe\n");
				break;
			case ASIA:
				fprintf(fptr, "Asia\n");
				break;
			case AMERICAS:
				fprintf(fptr, "Americas\n");
				break;
			case AUSTRALASIA:
				fprintf(fptr, "Australasia\n");
				break;
			}

			fprintf(fptr, "Travel class:     ");
			//Switch statemnt to print correct info to file depending on options number code
			switch(curr->travelClass)
			{
			case ECONOMY:
				fprintf(fptr, "Economy class\n");
				break;
			case PREMIUM:
				fprintf(fptr, "Premiuim economy class\n");
				break;
			case BUSSINES:
				fprintf(fptr, "Bussines class\n");
				break;
			case FIRST:
				fprintf(fptr, "First class\n");
				break;
			}

			fprintf(fptr, "Trips per year:   ");
			//Switch statemnt to print correct info to file depending on options number code
			switch(curr->travelPerYear)
			{
			case THREE_TIMES:
				fprintf(fptr, "3 per year\n");
				break;
			case FIVE_TIMES:
				fprintf(fptr, "5 per year\n");
				break;
			case MORE_THAN_FIVE:
				fprintf(fptr, "More than 5\n");
				break;
			}

			fprintf(fptr, "Duration:         ");
			//Switch statemnt to print correct info to file depending on options number code
			switch(curr->durationOfStay)
			{
			case ONE_DAY:
				fprintf(fptr, "1 day\n");
				break;
			case THREE_DAYS:
				fprintf(fptr, "Less than 3 days\n");
				break;
			case SEVEN_DAYS:
				fprintf(fptr, "Less than 7 days\n");
				break;
			case MORE_THAN_SEVEN:
				fprintf(fptr, "More than 7 days\n");
				break;
			}
			fprintf(fptr, "\n");
			curr = curr->next;
		}
	}
	fclose(fptr);
}

//Function for generating passenger stats
double generateStatsByTravelClassAndTravelOrigin(Passenger ** head, int travelClass, int option)
{
	Passenger * curr = *head;
	double count = 0;
	double percentage;

	//Variable to hold linked list length, used for percentage calculation
	double len = listLength(*head);

	//Traversing through linked list till end
	while(curr != NULL)
	{
		//If cuurent node travel class equals function argument travelClass and
		//If current node travel origin equals function argument option
		if(curr->travelClass == travelClass && curr->travelOrigin == option)
		{
			//Increment counter of passengers who match criteria
			count++;
		}
		curr = curr->next;
	}
	//Calcuating required percentage
	percentage = (double)(count / len) * 100.00;
	return percentage;
}

//Functon to generate passenger stats

double generateStatsByTravelClassAndDurationOfStay(Passenger ** head, int travelClass, int option)
{
	Passenger * curr = *head;
	double count = 0;
	double percentage;
	double len = listLength(*head);

	while(curr != NULL)
	{
		if(curr->travelClass == travelClass && curr->durationOfStay == option)
		{
			count++;
			printf("%lf\n", count);
		}
		curr = curr->next;
	}
	percentage = (double)(count / len) * 100.00;
	return percentage;
}
//Processing statistic criteria
void generateStatisticsProcess(Passenger * head, int statOptions[])
{
	//Depending on passed array of statistics cirteria options calling required function
	switch(statOptions[1])
	{
	case ECONOMY:
	case PREMIUM:
	case BUSSINES:
	case FIRST:
		if(statOptions[2] <= 5)
			printf("Percentage of passengers: %lf.2", generateStatsByTravelClassAndTravelOrigin(&head, statOptions[1], statOptions[2]));
		if(statOptions[2] >= 6)
			printf("Percentage of passengers: %lf.2", generateStatsByTravelClassAndDurationOfStay(&head, statOptions[1], statOptions[2]));
		break;
	case 0:
		if(statOptions[2] <= 5)
			//generateStatsByYearBornAndTravelOrigin(head, statOptions[1], statOptions[2]);
			if(statOptions[2] >= 6)
				//generateStatsByYearBornAndDurationOfStay(head, statOptions[1], statOptions[2]);
				break;
	}
}
//Function for main statistics menu
//Array passed as argument,which will hold chose statistic options
//options[0] = Travel class, or Born before 1980 criteria
//options[1] = holds travel class option
//options[2] = holds required option needed to calculate percentage
int * statisticsMenu(int options[])
{
	int choice;
	//Variable for checking if input is correct
	int correctInput = 1;

	//Do while loop for menu validation
	do
	{
		//Input correct by default
		correctInput = 1;
		choice = 0;

		printf("Choose criteria for statistics: \n");
		printf("1. Travel class. \n2. Born before 1980.\n");

		scanf("%d", &choice);

		if(choice == 1)
		{
			options[0] = 1;
			printf("Choose travel class: \n");
			printf("1. Economy.\n2. Premium economy.\n3. Business.\n 4. First.\n");

			scanf("%d", &choice);

			if(choice == 1)
			{
				options[1] = ECONOMY;
			}
			else if(choice == 2)
			{
				options[1] = PREMIUM;
			}
			else if(choice == 3)
			{
				options[1] = BUSSINES;
			}
			else if(choice == 4)
			{
				options[1] = FIRST;
			}
			else
			{
				printf("Incorrect input, try again.\n");
				correctInput = 0;
				continue;
			}
			options[2] = statisticsSecondMenu();
		}
		else if(choice == 2)
		{
			options[0] = 2;
			options[1] = 0;
			options[2] = statisticsSecondMenu();
		}
		else
		{
			//If input is incorrect, using continue to evaluate loop condition
			printf("Incorrect input.\n");
			correctInput = 0;
			continue;
		}
	}
	while(correctInput != 1);

	return options;
}

//Separte function for statistics secon menu with percentage choices
int statisticsSecondMenu()
{
	int choice;

	printf("Choose one of the available statistics: \n");
	printf("1. %% of passengers who travel from the UK\n");
	printf("2. %% of passengers who travel from the Rest of Europe");
	printf("3. %% of players who travel from the Asia\n");
	printf("4. %% of passengers who travel from the Americas\n");
	printf("5. %% of passengers who travel from the Australasia\n");
	printf("6. %% of passengers who spent on average one day in Ireland\n");
	printf("7. %% of passengers who spent on average less than 3 days in Ireland\n");
	printf("8. %% of passengers who spent on average less than 7 days in Ireland\n");
	printf("9. %% of passengers who spent on average more than 7 days in Ireland\n");

	scanf("%d", &choice);
	//Geting correct choice value
	if(choice == 6) choice = ONE_DAY;
	if(choice == 7) choice = THREE_DAYS;
	if(choice == 8) choice = SEVEN_DAYS;
	if(choice == 9) choice = MORE_THAN_SEVEN;

	return choice;
}

//Email validation function
int validateEmail(char email[])
{
	const char s[2] = "@.";
	const char com[3] = "com";
	//Pointer to strings for storing tokens
	char *tok[3];
	char *token;
	int tokenCount;
	//Taking first token
	token = strtok(email, s);
	tokenCount = 0;

	//Itertating through other tokens
	while( token != NULL)
	{
		//Saving token for validation
		tok[tokenCount] = token;

		token = strtok(NULL, s);
		//Increasing token count for tok array index
		if(token != NULL)tokenCount++;
	}
	//Checking if token count is correct, or if last token (tok[2]) equals "com"
	//If not - email format is incorrect
	if(tokenCount != 2 || strcmp(tok[2], com) != 0)
	{
		printf("Email format is incorrect, try again.\n");
		return 0;
	}
	return 1;
}

//Atuhentification function
int login()
{
	//Variables to hold user entered login details
	char loginName[10];
	char password[6];
	//Opening auth file with login details
	FILE * fptr;
	fptr = fopen("auth.dat", "r");

	if(fptr == NULL)
	{
		printf("Authentification file could not be opened;");
		exit(0);
	}
	else
	{
		printf("Login: ");
		scanf("%s", loginName);
		printf("Password: ");
		scanf("%s", password);

		//Loading login details from file
		while(!feof(fptr))
		{
			//Vars to hold login details from file
			char loginFromFile[10];
			char passwordFromFile[6];

			fscanf(fptr, "%s %s", loginFromFile, passwordFromFile);
			//Comparing user entered login details and login details from file
			//If correct - login succesful
			if(strcmp(loginName, loginFromFile) == 0 && strcmp(password, passwordFromFile) == 0)
			{
				printf("Login successful.\n\n");
				return 1;
			}
		}
		//If login details not correct
		printf("Login or password incorrect. Try again.\n\n");
		return 0;
	}
}

//Loading passenger file
void loadPassengerListFile(Passenger ** head)
{
	FILE *fptr;
	fptr = fopen("passengerList.dat", "r");

	if (fptr == NULL)
	{
		printf("The file could not be opened\n");
	}
	else
	{
		printf("Loading data file.\n");

		while (!feof(fptr))
		{
			//Allocating dynamic memory for passenger struct and populating it from file
			Passenger * readPassenger = (Passenger *)malloc(sizeof(Passenger));
			fscanf(fptr, "%d %s %s %d %s %d %d %d %d",
					&readPassenger->passportNumber, readPassenger->firstName, readPassenger->secondName,
					&readPassenger->DOB, readPassenger->email, &readPassenger->travelOrigin, &readPassenger->travelClass,
					&readPassenger->travelPerYear, &readPassenger->durationOfStay);

			readPassenger->next = *head;
			*head = readPassenger;
		}
		printf("Data loaded successfully.\n\n");
		fclose(fptr);
	}
}

//Add passenger function
void addPassenger(Passenger** head)
{
	Passenger* newPassenger;
	int passportNum;
	int passengerIndex;
	int choice;

	//Alocating space for new passenger
	newPassenger = (Passenger*)malloc(sizeof(Passenger));

	//Assking for user input for purpose of checking if user with this passport already exists
	printf("Enter new passengers passport number: ");
	scanf("%d", &passportNum);

	//Checking is passenger with passport already exists
	passengerIndex = searchPassengerList(*head, passportNum, NULL);

	//If passenger index is higher than 0, means passenger exits and passengerIndex is position at passengers linked list
	if(passengerIndex > 0)
	{
		printf("\nPassenger with this passport number already exists in database.");
		printf("\nWould you like to update details?");
		printf("\n1. Yes 2. No");

		scanf("%d", &choice);

		if(choice == 1)
		{
			//Update passenger at position
			updatePassenger(head, passengerIndex);
		}
		else
		{
			return;
		}
	}
	//If passenger with passport number dont exist, create new passenger
	else
	{
		//Calling create passenger function
		newPassenger = createPassenger(*head, passportNum);
		newPassenger->next = *head;
		*head = newPassenger;
	}
	//Sorting list to place passenger at correct position depending on its passport number
	sortList(head);
}

//Create passenger function
//This functionn returns passeger type, whihch addPassenger functtion uses to add to list
struct Passenger * createPassenger(Passenger * head, int passportNum)
{
	//Dynamically allocating space for passenger to be created
	struct Passenger * newPassenger = (Passenger*)malloc(sizeof(Passenger));

	if(newPassenger == NULL)
	{
		puts("Error creating new passenger");
		return 0;
	}
	else
	{
		newPassenger->passportNumber = passportNum;

		printf("Enter first name: ");
		scanf("%s", newPassenger->firstName);
		printf("Enter second name: ");
		scanf("%s", newPassenger->secondName);
		printf("Enter date of birth in format ddmmyyyy: ");
		scanf("%d", &newPassenger->DOB);
		do
		{
			printf("Enter passengers email: ");
			scanf("%s", newPassenger->email);
		}
		while(validateEmail(newPassenger->email) != 1);
		printf("Enter passengers origin of travel: \n");
		printf("1. UK\n2. Rest of Europe \n3. Asia \n4. Americas \n5. Australasia\n");
		scanf("%d", &newPassenger->travelOrigin);
		printf("Enter passengers travel class: \n");
		printf("1. Economy \n2. Premiuim economy \n3. Bussines \n4. First\n");
		scanf("%d", &newPassenger->travelClass);
		printf("Passengers trips per year to Ireland: \n");
		printf("1. Less than 3 times per year. \n2. Less than 5 times per year. \n3. More than 5 times per year.\n");
		scanf("%d", &newPassenger->travelPerYear);
		printf("Enter passengers average duration of stay: \n");
		printf("1. One day \n2. Less than 3 days \n 3.Less than 7 days \n4.More than 7 days\n");
		scanf("%d", &newPassenger->durationOfStay);
	}
	return newPassenger;
}

void displayList(Passenger* head)
{
	Passenger* curr;
	int passengerNumber = 0;
	curr = head;

	if(curr == NULL)
	{
		printf("\nData file is empty.\n\n");
		return;
	}

	while (curr != NULL)
	{
		passengerNumber++;
		printf("Passenger No.%d:\n", passengerNumber);
		printf("Passport number:  %d\n", curr->passportNumber);
		printf("First name:       %s\n", curr->firstName);
		printf("Last name:        %s\n", curr->secondName);
		printf("DOB:              %d\n", curr->DOB);
		printf("Email:            %s\n", curr->email);
		printf("Origin of travel: ");

		switch(curr->travelOrigin)
		{
		case UK:
			printf("UK\n");
			break;
		case RESTOFEUROPE:
			printf("Rest of Europe\n");
			break;
		case ASIA:
			printf("Asia\n");
			break;
		case AMERICAS:
			printf("Americas\n");
			break;
		case AUSTRALASIA:
			printf("Australasia\n");
			break;
		}

		printf("Travel class:     ");

		switch(curr->travelClass)
		{
		case ECONOMY:
			printf("Economy class\n");
			break;
		case PREMIUM:
			printf("Premiuim economy class\n");
			break;
		case BUSSINES:
			printf("Bussines class\n");
			break;
		case FIRST:
			printf("First class\n");
			break;
		}

		printf("Trips per year:   ");

		switch(curr->travelPerYear)
		{
		case THREE_TIMES:
			printf("3 per year\n");
			break;
		case FIVE_TIMES:
			printf("5 per year\n");
			break;
		case MORE_THAN_FIVE:
			printf("More than 5\n");
			break;
		}

		printf("Duration:         ");

		switch(curr->durationOfStay)
		{
		case ONE_DAY:
			printf("1 day\n");
			break;
		case THREE_DAYS:
			printf("Less than 3 days\n");
			break;
		case SEVEN_DAYS:
			printf("Less than 7 days\n");
			break;
		case MORE_THAN_SEVEN:
			printf("More than 7 days\n");
			break;
		}
		printf("\n");
		curr = curr->next;
	}
}
//Function to search for passenger in list, using passport num or full name
int searchPassengerList(Passenger * head, int passportNum, char fullName[])
{
	Passenger * curr = head;
	int position = 0;
	int i;
	//If pasport num not equals 0, we use it for search
	if(passportNum != 0)
	{
		while(curr != NULL)
		{
			position++;

			if(passportNum == curr->passportNumber) return position;

			curr = curr->next;
		}
	}
	//If passportNum = 0, we use fullName to search for passenger
	else
	{
		//Change fullName to uppercase
		for(i = 0; i < strlen(fullName); i++) fullName[i] = toupper(fullName[i]);

		while(curr != NULL)
		{
			char first[20] = "\0";
			char last[20] = "\0";
			char full[40] = "\0";
			//Store first and last names from list
			strcpy(first,curr->firstName);
			strcpy(last,curr->secondName);
			strcpy(full,strcat(first,last));

			position++;
			//Chanage full name from list to upper case
			for(i = 0; i < strlen(full); i++) full[i] = toupper(full[i]);
			//Compare functtion argumet fullName and full name from list
			if(strcmp(fullName,full) == 0)
			{
				return position;
			}
			curr = curr->next;
		}
	}
	return -1;
}

//Sorting function
//Using bubble sort
//The easy way to sort linked list
//Just to change data of nodes to correct positions
void sortList(Passenger ** head)
{
	Passenger *temp3 = (Passenger*)malloc(sizeof(Passenger));
	Passenger *temp1;
	Passenger *temp2;

	for(temp1=*head;temp1!=NULL;temp1=temp1->next)
	{
		for(temp2=temp1->next;temp2!=NULL;temp2=temp2->next)
		{
			if(temp2->passportNumber < temp1->passportNumber)
			{
				temp3 = swapNodeData(temp3,temp1);
				temp1 = swapNodeData(temp1, temp2);
				temp2 = swapNodeData(temp2, temp3);
			}
		}
	}
}

//Function to swap node data specified by sortList function
Passenger * swapNodeData(Passenger * temp1, Passenger * temp2)
{
	temp1->passportNumber = temp2->passportNumber;
	strcpy(temp1->firstName, temp2->firstName);
	strcpy(temp1->secondName, temp2->secondName);
	temp1->DOB = temp2->DOB;
	strcpy(temp1->email, temp2->email);
	temp1->travelOrigin = temp2->travelOrigin;
	temp1->travelClass = temp2->travelClass;
	temp1->travelPerYear = temp2->travelPerYear;
	temp1->durationOfStay = temp2->durationOfStay;

	return temp1;
}

//Update function
//Find passenger at correct position and update
void updatePassenger(Passenger ** head, int position)
{
	Passenger * newPassenger = *head;
	int passportNum;

	for (int i = 0;i < position - 1;i++)
	{
		newPassenger = newPassenger->next;
	}
	printf("Enter new passengers passport number: ");
	scanf("%d", &passportNum);
	newPassenger->passportNumber = passportNum;

	printf("Enter first name: ");
	scanf("%s", newPassenger->firstName);
	printf("Enter second name: ");
	scanf("%s", newPassenger->secondName);
	printf("Enter date of birth in format ddmmyyyy: ");
	scanf("%d", &newPassenger->DOB);
	do
	{
		printf("Enter passengers email: ");
		scanf("%s", newPassenger->email);
	}
	while(validateEmail(newPassenger->email) != 1);
	printf("Enter passengers origin of travel: \n");
	printf("1. UK\n2. Rest of Europe \n3. Asia \n4. Americas \n5. Australasia\n");
	scanf("%d", &newPassenger->travelOrigin);
	printf("Enter passengers travel class: \n");
	printf("1. Economy \n2. Premiuim economy \n3. Bussines \n4. First\n");
	scanf("%d", &newPassenger->travelClass);
	printf("Passengers trips per year to Ireland: \n");
	printf("1. Less than 3 times per year. \n2. Less than 5 times per year. \n3. More than 5 times per year.\n");
	scanf("%d", &newPassenger->travelPerYear);
	printf("Enter passengers average duration of stay: \n");
	printf("1. One day \n2. Less than 3 days \n 3.Less than 7 days \n4.More than 7 days\n");
	scanf("%d", &newPassenger->durationOfStay);


	sortList(head);
}



void displayPassenger(Passenger ** head, int update)
{
	int choice;
	int position;
	int passportNum = 0;
	char firstName[20], secondName[20];

	printf("Find passenger using:\n");
	printf("1. Passport number.\n");
	printf("2. Full name.\n");

	scanf("%d", &choice);

	if(choice == 1)
	{
		printf("Enter passengers passport number: ");
		scanf("%d", &passportNum);

		position = searchPassengerList(*head,passportNum, "");
		printPassengerDetails(*head, position);

		if(update == 1) updatePassenger(head, position);
	}
	else if(choice == 2)
	{
		printf("Enter passengers first name: ");
		scanf("%s", firstName);

		printf("Enter passengers second name: ");
		scanf("%s", secondName);

		position = searchPassengerList(*head, passportNum, strcat(firstName, secondName));
		printPassengerDetails(*head, position);

		if(update == 1) updatePassenger(head, position);
	}
}

void printPassengerDetails(Passenger * head, int position)
{
	Passenger * printPassenger = head;

	for (int i = 0;i < position - 1;i++)
	{
		printPassenger = printPassenger->next;
	}

	printf("Passport number: %d\n", printPassenger->passportNumber);
	printf("First name: %s\n", printPassenger->firstName);
	printf("Last name: %s\n", printPassenger->secondName);
}

//Delete passenger function
//If else statement for correcet operation for deletion of passenger
void deletePassenger(Passenger ** head)
{
	Passenger* curr;
	Passenger* prev_curr;
	int i;

	int passportNum;
	int position;

	printf("Enter passengers passport number to delete: ");
	scanf("%d", &passportNum);

	position = searchPassengerList(*head, passportNum, NULL);

	curr = *head;

	if(position == 1){

		curr = *head;

		*head = curr->next;

		free(curr);
		return;
	}

	if(position == listLength(*head))
	{
		while (curr->next != NULL)
		{
			prev_curr = curr;
			curr = curr->next;
		}
		prev_curr->next = NULL;
		free(curr);

		return;
	}

	for (i = 0;i < position - 1;i++)
	{
		prev_curr = curr;
		curr = curr->next;
	}
	prev_curr->next = curr->next;
	free(curr);

}

//Function to get linked list length
int listLength(Passenger * head)
{
	Passenger * curr = head;
	int len = 0;

	while (curr != NULL)
	{
		len++;
		curr = curr->next;
	}
	return len;
}

